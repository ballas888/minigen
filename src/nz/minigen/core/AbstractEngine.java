package nz.minigen.core;

import com.jogamp.opengl.*;
import com.jogamp.opengl.awt.GLCanvas;
import com.jogamp.opengl.util.glsl.ShaderUtil;
import nz.minigen.input.Keyboard;
import nz.minigen.log.GLTraceLog;

import javax.swing.*;
import javax.swing.plaf.basic.BasicInternalFrameTitlePane;

/**
 * Created by Richard on 27/03/2016.
 */
public abstract class AbstractEngine extends JFrame implements GLEventListener {
    private final double NANO = 1000000000;

    private String frameName;
    private EngineConfig config;

    private double gameHertz;
    private double timeBetweenUpdates;
    private int maxUpdatesBeforeRender;
    private double targetFps;
    private double targetTimeBetweenRenders;

    public void start(EngineConfig config, String frameName){
        this.config = config;
        this.frameName = frameName;

        this.gameHertz = 30.0;
        this.timeBetweenUpdates = NANO/gameHertz;
        this.maxUpdatesBeforeRender = 5;
        this.targetFps = 60;
        this.targetTimeBetweenRenders = NANO/targetFps;


    }

    private void loop(){

    }

    @Override
    public void init(GLAutoDrawable glAutoDrawable) {

    }

    @Override
    public void dispose(GLAutoDrawable glAutoDrawable) {

    }

    @Override
    public void display(GLAutoDrawable glAutoDrawable) {

    }

    @Override
    public void reshape(GLAutoDrawable glAutoDrawable, int i, int i1, int i2, int i3) {

    }
}