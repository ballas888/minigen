package nz.minigen.core;

import com.jogamp.common.util.InterruptSource;
import com.jogamp.opengl.*;
import com.jogamp.opengl.awt.GLCanvas;
import com.jogamp.opengl.util.glsl.ShaderUtil;
import nz.minigen.input.Keyboard;
import nz.minigen.log.GLTraceLog;

import javax.swing.*;

/**
 * Created by Richard on 27/03/2016.
 */
public abstract class AbstractEngineOld extends JFrame implements GLEventListener{

    private float updateTicksPerSecond;
    private float updateSkipTicks;
    private int updateMaxFrameSkip;

    private double updateNextTick = 0.0f;
    private int updateLoops = 0;
    private int updateCount = 0;
    private long updateIterationCount = 0;

    private float renderTicksPerSecond;
    private float renderSkipTicks;
    private int renderMaxFrameSkip;

    private double renderNextTick = 0.0f;
    private int renderLoops = 0;
    private int renderCount = 0;
    private long renderIterationCount = 0;

    private long startTime = 0;
    protected float interpolation = 1f;
    private String frameName;


    private boolean running = false;

    protected EngineConfig config;
    protected GLCanvas glCanvas;
    protected Keyboard keyboard = new Keyboard();
    protected GLAutoDrawable drawable;

    public void start(){
        this.start(new EngineConfig(), "Default");
    }

    public void start(String name){
        this.start(new EngineConfig(), name);
    }

    public void start(EngineConfig conf){
        this.start(conf,"Default");
    }

    public void start(EngineConfig engConfig, String title){
        this.frameName = title;
        this.config = engConfig;

        this.updateTicksPerSecond = config.UPDATE_PER_SECOND;
        this.updateSkipTicks = 1000.0f/updateTicksPerSecond;
        this.updateMaxFrameSkip = config.UPDATE_MAX_FRAME_SKIP;
        this.renderTicksPerSecond = config.RENDER_PER_SECOND;
        this.renderSkipTicks = 1000.0f/renderTicksPerSecond;
        this.renderMaxFrameSkip = config.RENDER_MAX_FRAME_SKIP;

        GLProfile profile = GLProfile.get(GLProfile.GL3);
        GLCapabilities capabilities = new GLCapabilities(profile);
        this.glCanvas = new GLCanvas(capabilities);
        this.glCanvas.addGLEventListener(this);
        this.glCanvas.addKeyListener(keyboard);

        getContentPane().add(glCanvas);
        setSize(config.WIDTH, config.HEIGHT);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setVisible(true);
        setResizable(false);

        glCanvas.requestFocusInWindow();
    }

    @Override
    public void init(GLAutoDrawable glAutoDrawable) {
        GL draw = glAutoDrawable.getGL().getGL3();
        if(config.DEBUG){
            try {
                // Debug ..
                draw = draw.getContext().setGL( GLPipelineFactory.create("com.jogamp.opengl.Debug", null, draw, null) );
            } catch (Exception e) {e.printStackTrace();}
        }

        if(config.TRACE) {
            try {
                // Trace ..
                draw = draw.getContext().setGL( GLPipelineFactory.create("com.jogamp.opengl.Trace", null, draw, new Object[] { new GLTraceLog(System.err)} ) );
            } catch (Exception e) {e.printStackTrace();}
        }

        GL3 gl = draw.getGL3();

        //GL Settings
        gl.setSwapInterval(config.VSYNC?1:0);
        gl.glClearColor(0.392f, 0.584f, 0.929f, 1.0f);
        gl.glEnable(GL3.GL_DEPTH_TEST);

        System.out.println("------------------------------------------------------------------");
        System.out.println("Entering Initialization!");
        System.out.println("GL Profile: "+ gl.getGLProfile());
        System.out.println("GL: "+gl);
        System.out.println("GL Version: "+gl.glGetString(GL.GL_VERSION));
        System.out.println("Vsync: "+gl.getSwapInterval());
        System.out.println("isShaderCompilerAvailible: "+ ShaderUtil.isShaderCompilerAvailable(gl));
        System.out.println("Program Ready!");
        System.out.println("------------------------------------------------------------------\n");


        drawable = glAutoDrawable;
        initialize(gl);

        Thread loopThread = new Thread(new Runnable() {
            @Override
            public void run() {
                loop();
            }
        });
        loopThread.setName("Loop Thread");
        loopThread.start();

    }

    @Override
    public void dispose(GLAutoDrawable glAutoDrawable) {
        running = false;
        GL3 gl = glAutoDrawable.getGL().getGL3();
        terminate(gl);
    }

    @Override
    public void display(GLAutoDrawable glAutoDrawable) {

        GL3 gl = glAutoDrawable.getGL().getGL3();
        gl.glClear(GL3.GL_COLOR_BUFFER_BIT|GL3.GL_DEPTH_BUFFER_BIT);
        interpolation = (float)((time()+updateSkipTicks-updateNextTick)/updateSkipTicks);
        renderLoop(gl,interpolation, (float)time(), renderIterationCount);
        renderCount++;
        renderIterationCount++;
    }

    @Override
    public void reshape(GLAutoDrawable glAutoDrawable, int x, int y, int width, int height) {
        GL3 gl = glAutoDrawable.getGL().getGL3();
        resize(gl, x, y, width, height);
    }

    private void loop(){

        running = true;
        updateNextTick = time();
        renderNextTick = time();
        double oldTime = time();

        while(running){
            updateLoops = 0;
            renderLoops = 0;

            keyboard.update();

            while(time() > updateNextTick && updateLoops < updateMaxFrameSkip){
                logicLoop(drawable.getGL().getGL3(), (float)time(), updateIterationCount);
                updateNextTick+=updateSkipTicks;
                updateLoops++;
                updateIterationCount++;
                updateCount++;
            }

            while(time() > renderNextTick && renderLoops < renderMaxFrameSkip){
                //glCanvas.repaint();
                drawable.display();
                renderNextTick+=renderSkipTicks;
                renderLoops++;
            }

            if(time() - oldTime > 1000.0){
                //System.out.println("FPS: "+renderCount+" UPS: "+updateCount);
                setTitle(frameName+"     FPS: "+renderCount+" UPS: "+updateCount);
                updateCount = 0;
                renderCount = 0;
                oldTime += 1000.0;
            }

        }
    }

    public double time(){
        if(startTime == 0.0){
            startTime = System.nanoTime();
            return 0.0f;
        }
        long currentTime = System.nanoTime();
        return (currentTime - startTime)/1000000.0;
    }

    public abstract void initialize(GL3 gl);
    public abstract void logicLoop(GL3 gl,float time, long count);
    public abstract void renderLoop(GL3 gl,float inter, float time, long count);
    public abstract void resize(GL3 gl, int x, int y, int width, int height);
    public abstract void terminate(GL3 gl);
}
