package nz.minigen.input;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

/**
 * Created by Richard on 26-Mar-16.
 */
public class Keyboard implements KeyListener {

    public enum KeyCode{
        KEY_UP(38),
        KEY_LEFT(37),
        KEY_RIGHT(39),
        KEY_DOWN(40);

        public final int v;
        KeyCode(int value){
            this.v = value;
        }
    }

    public boolean keyNum1 = false;
    public boolean keyNum2 = false;
    public boolean keyUp = false;
    public boolean keyDown = false;
    public boolean keyLeft = false;
    public boolean keyRight = false;

    private boolean keys[] = new boolean[256];

    public void update(){
        this.keyUp = keys[KeyCode.KEY_UP.v];
        this.keyDown = keys[KeyCode.KEY_DOWN.v];
        this.keyLeft = keys[KeyCode.KEY_LEFT.v];
        this.keyRight = keys[KeyCode.KEY_RIGHT.v];
    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {
        int key = e.getKeyCode();
        if(key < keys.length){
            keys[key] = true;
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {
        int key = e.getKeyCode();
        if(key < keys.length){
            keys[key] = false;
        }
    }
}
