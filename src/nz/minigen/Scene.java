package nz.minigen;

import com.jogamp.opengl.GL3;

/**
 * Created by Richard on 26-Mar-16.
 */
public abstract class Scene {
    private String sceneName;
    private boolean isInit = false;
    public void setSceneName(String name){
        this.sceneName = name;
    }

    public String getSceneName(){
        return this.sceneName;
    }

    public void isInit(boolean bool){
        this.isInit = bool;
    }

    public boolean isInit(){
        return this.isInit;
    }

    public abstract void init(GL3 gl);
    public abstract void update();
    public abstract void render(GL3 gl, float inter, float time, long count);
    public abstract void dispose(GL3 gl);
    public abstract void reshape(GL3 gl,int x, int y, int width, int height);
}
