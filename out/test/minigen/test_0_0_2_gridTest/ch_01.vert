#version 450 core

layout (location = 0) in vec3 in_position;
layout (location = 1) uniform mat4 u_pv_matrix;
layout (location = 2) uniform mat4 u_m_matrix;

void main(void)
{
    //gl_Position = u_pv_matrix * u_m_matrix * vec4(in_position,1.0);
    gl_Position = u_m_matrix * vec4(in_position,1.0);
}