#version 450 core

// Input from the vertex shader
in vec4 vs_color;

// Output to the framebuffer
out vec4 color;
void main(void)
{
    // Simply assign the color we were given by the vertex shader to our output
    color = vec4(sin(gl_FragCoord.x * 0.25) * cos(gl_FragCoord.y * 0.15),
            cos(gl_FragCoord.y * 0.25) * sin(gl_FragCoord.x * 0.15),
            sin(gl_FragCoord.x * 0.15) * cos(gl_FragCoord.y * 0.15),
            1.0);

}
