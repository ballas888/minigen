package nz.minigen;

import com.jogamp.opengl.GL4;
import nz.minigen.input.Keyboard;

/**
 * Created by Ballas on 1/12/2016.
 */
public abstract class AbstractCallback {
    private String title = "Default Title";
    private float canvasWidth = 800;
    private float canvasHeight = 600;
    protected Keyboard keyboard = new Keyboard();

    public void updateKeyboard(){
        this.keyboard.update();
    }


    public String getTitle(){
        return this.title;
    }

    public void setTitle(String title){
        this.title = title;
    }

    public abstract void init(GL4 gl);
    public abstract void update();
    public abstract void render(GL4 gl, float inter, float time, long count);
    public abstract void dispose(GL4 gl);
    public abstract void reshape(GL4 gl,int x, int y, int width, int height);

    public float getCanvasHeight(){return canvasHeight;}
    public float getCanvasWidth(){ return canvasWidth;}
    public void setCanvasWidth(float width){this.canvasWidth = width;}
    public void setCanvasHeight(float height){this.canvasHeight = height;}
}
