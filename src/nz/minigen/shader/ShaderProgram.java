package nz.minigen.shader;

import com.jogamp.opengl.GL3;
import com.jogamp.opengl.GL4;
import nz.minigen.loader.TextLoader;
import nz.minigen.log.Log;

import java.nio.ByteBuffer;
import java.nio.IntBuffer;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Ballas on 1/14/2016.
 */
public class ShaderProgram {
    public int id;
    private boolean isValid = false;

    private Map<String,Integer> uniforms = new HashMap<>();

    private enum ShaderType{
        VERTEX_SHADER("VERTEX_SHADER"),
        FRAGMENT_SHADER("FRAGMENT_SHADER");
        private final String value;
        ShaderType(String type){
            this.value = type;
        }
        public String getValue(){
            return this.value;
        }
    }

    public int getId(){
        return this.id;
    }

    public void dispose(GL3 gl){
        gl.glUseProgram(0);
        gl.glDeleteProgram(id);
    }

    public ShaderProgram(GL3 gl, String vertLoc, String fragLoc){
        String[] vertSource = new String[1];
        vertSource[0] = TextLoader.load(vertLoc);
        String[] fragSource = new String[1];
        fragSource[0] = TextLoader.load(fragLoc);

        if(vertSource[0].isEmpty() || fragSource[0].isEmpty()){
            Log.e("Shader Source is Empty!");
            return;
        }

        int vertId = gl.glCreateShader(GL3.GL_VERTEX_SHADER);
        int fragId = gl.glCreateShader(GL3.GL_FRAGMENT_SHADER);

        gl.glShaderSource(vertId,1,vertSource,null,0);
        gl.glCompileShader(vertId);
        checkShaderError(gl,vertId,ShaderType.VERTEX_SHADER);

        gl.glShaderSource(fragId, 1, fragSource, null, 0);
        gl.glCompileShader(fragId);
        checkShaderError(gl,fragId,ShaderType.FRAGMENT_SHADER);

        id = gl.glCreateProgram();
        gl.glAttachShader(id, vertId);
        gl.glAttachShader(id, fragId);
        gl.glLinkProgram(id);
        boolean noError = checkForProgramError(gl);

        gl.glDeleteShader(vertId);
        gl.glDeleteShader(fragId);

        if(noError){
            isValid = true;
            System.out.println("Created Program ("+id+"):\n\t"+vertLoc+"\n\t"+fragLoc);
        }else{
            Log.e("Shader Program Failed!\n\t"+vertLoc+"\n\t"+fragLoc);
        }
    }

    public void use(GL3 gl){
        if(isValid){
            gl.glUseProgram(id);
        }else{
            Log.e("Warning! Shader Program Not Valid");
        }
    }

    public boolean addUniform(GL3 gl, String uName){
        int location = gl.glGetUniformLocation(this.id,uName);
        if(location < 0 ){
            Log.e("Uniform "+uName+" Not part of shader program "+id);
            return false;
        }else{
            uniforms.put(uName,location);
            System.out.println("Added uniform ("+id+"): "+uName);
        }
        return true;
    }

    public int getUniform(String uName){
        Object obj = uniforms.get(uName);
        if(obj != null){
            return (int)obj;
        }else{
            Log.e("Warning! "+uName+" does not exist in shader program: "+id+"\n\tReturning -1");
            return -1;
        }
    }

    private boolean checkForProgramError(GL3 gl){
        IntBuffer intValue = IntBuffer.allocate(1);
        gl.glGetProgramiv(id,GL3.GL_INFO_LOG_LENGTH,intValue);
        int lengthWithNull = intValue.get();
        if(lengthWithNull > 0){
            ByteBuffer infoLog = ByteBuffer.allocate(lengthWithNull);

            intValue.flip();
            gl.glGetProgramInfoLog(id,lengthWithNull,intValue,infoLog);

            int actualLength = intValue.get();
            byte[] infoBytes = new byte[actualLength];
            infoLog.get(infoBytes);

            StackTraceElement[] trace = Thread.currentThread().getStackTrace();
            Log.e("Program Linking Failed!");
            Log.e("Program Info Log: \n\t\t"+new String(infoBytes));
            Log.e("Stack Trace:");
            for(int i = 0; i < trace.length && i < 5;i++){
                Log.e("\t\t"+trace[i]);
            }
            Log.e("...");
            return false;
        }
        return true;
    }

    private boolean checkShaderError(GL3 gl, int id, ShaderType type){
        IntBuffer intValue = IntBuffer.allocate(1);
        gl.glGetShaderiv(id, GL3.GL_INFO_LOG_LENGTH,intValue);
        int lengthWithNull = intValue.get();
        if(lengthWithNull > 0){
            ByteBuffer infoLog = ByteBuffer.allocate(lengthWithNull);

            intValue.flip();
            gl.glGetShaderInfoLog(id,lengthWithNull,intValue,infoLog);

            int actualLength = intValue.get();
            byte[] infoBytes = new byte[actualLength];
            infoLog.get(infoBytes);

            StackTraceElement[] trace = Thread.currentThread().getStackTrace();
            Log.e(type.getValue()+" Failed to Compile!");
            Log.e("Shader Info Log: \n\t\t"+new String(infoBytes));
            Log.e("Stack Trace:");
            for(int i = 0; i < trace.length && i < 5;i++){
                Log.e("\t\t"+trace[i]);
            }
            Log.e("...");
            return false;
        }
        return true;
    }
}
