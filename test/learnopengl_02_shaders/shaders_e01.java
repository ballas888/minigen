package learnopengl_02_shaders;

import com.jogamp.common.nio.Buffers;
import com.jogamp.opengl.GL4;
import nz.minigen.AbstractCallback;
import nz.minigen.JOGLWrapper;
import nz.minigen.shader.ShaderProgram;

import java.nio.FloatBuffer;

/**
 * Created by Richard on 13/03/2016.
 */
public class shaders_e01 extends AbstractCallback {

    FloatBuffer vertices = Buffers.newDirectFloatBuffer(new float[]{
        0.5f, -0.5f, 0.0f,  1.0f, 0.0f, 0.0f,  // Bottom Right
        -0.5f, -0.5f, 0.0f,  0.0f, 1.0f, 0.0f,  // Bottom Left
        0.0f,  0.5f, 0.0f,  0.0f, 0.0f, 1.0f
    });

    int vbos[] = new int[1];
    int vaos[] = new int[1];
    ShaderProgram program;
    int u_color;

    @Override
    public void init(GL4 gl) {
        program = new ShaderProgram(gl, "/learnopengl_02_shaders/shader/v_e01.vert", "/learnopengl_02_shaders/shader/f_01.frag");
        u_color = gl.glGetUniformLocation(program.id,"u_color");

        gl.glGenVertexArrays(1,vaos, 0);
        gl.glGenBuffers(1, vbos, 0);

        gl.glBindVertexArray(vaos[0]);
        {
            gl.glBindBuffer(GL4.GL_ARRAY_BUFFER, vbos[0]);
            gl.glBufferData(GL4.GL_ARRAY_BUFFER,vertices.capacity()*Buffers.SIZEOF_FLOAT,vertices,GL4.GL_STATIC_DRAW);

            gl.glVertexAttribPointer(0,3,GL4.GL_FLOAT,false,Buffers.SIZEOF_FLOAT*6,0);
            gl.glEnableVertexAttribArray(0);

            gl.glVertexAttribPointer(1,3,GL4.GL_FLOAT,false,Buffers.SIZEOF_FLOAT*6,3*4);
            gl.glEnableVertexAttribArray(1);
        }
        gl.glBindVertexArray(0);
    }

    @Override
    public void update() {

    }

    @Override
    public void render(GL4 gl, float inter, float time, long count) {
        program.use(gl);
        gl.glUniform3f(u_color,1.0f,1.0f,1.0f);
//
        gl.glBindVertexArray(vaos[0]);
        gl.glDrawArrays(GL4.GL_TRIANGLES, 0, 3);
        gl.glBindVertexArray(0);
    }

    @Override
    public void dispose(GL4 gl) {

    }

    @Override
    public void reshape(GL4 gl, int x, int y, int width, int height) {

    }

    public static void main(String[] args){
        shaders_e01 engine = new shaders_e01();
        engine.setTitle("shaders.shaders_02");
        JOGLWrapper wrapper = new JOGLWrapper(engine);
    }
}
