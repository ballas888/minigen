package learnopengl_04_coords;

import com.jogamp.common.nio.Buffers;
import com.jogamp.opengl.GL4;
import nz.minigen.AbstractCallback;
import nz.minigen.JOGLWrapper;
import nz.minigen.shader.ShaderProgram;
import org.joml.Matrix4f;
import org.joml.Vector3f;

import java.nio.FloatBuffer;

/**
 * Created by Richard on 13/03/2016.
 */
public class coords_03 extends AbstractCallback {

    private final float PI = (float)Math.PI;

    FloatBuffer vertices = Buffers.newDirectFloatBuffer(new float[]{
            -0.5f, -0.5f, -0.5f,  0.0f, 0.0f, 0.0f,
            0.5f, -0.5f, -0.5f,  1.0f, 0.0f, 0.0f,
            0.5f,  0.5f, -0.5f,  1.0f, 1.0f, 0.0f,
            0.5f,  0.5f, -0.5f,  1.0f, 1.0f, 0.0f,
            -0.5f,  0.5f, -0.5f,  0.0f, 1.0f, 0.0f,
            -0.5f, -0.5f, -0.5f,  0.0f, 0.0f, 0.0f,

            -0.5f, -0.5f,  0.5f,  0.0f, 0.0f, 0.0f,
            0.5f, -0.5f,  0.5f,  1.0f, 0.0f, 0.0f,
            0.5f,  0.5f,  0.5f,  1.0f, 1.0f, 0.0f,
            0.5f,  0.5f,  0.5f,  1.0f, 1.0f, 0.0f,
            -0.5f,  0.5f,  0.5f,  0.0f, 1.0f, 0.0f,
            -0.5f, -0.5f,  0.5f,  0.0f, 0.0f, 0.0f,

            -0.5f,  0.5f,  0.5f,  1.0f, 0.0f, 0.0f,
            -0.5f,  0.5f, -0.5f,  1.0f, 1.0f, 0.0f,
            -0.5f, -0.5f, -0.5f,  0.0f, 1.0f, 0.0f,
            -0.5f, -0.5f, -0.5f,  0.0f, 1.0f, 0.0f,
            -0.5f, -0.5f,  0.5f,  0.0f, 0.0f, 0.0f,
            -0.5f,  0.5f,  0.5f,  1.0f, 0.0f, 0.0f,

            0.5f,  0.5f,  0.5f,  1.0f, 0.0f, 0.0f,
            0.5f,  0.5f, -0.5f,  1.0f, 1.0f, 0.0f,
            0.5f, -0.5f, -0.5f,  0.0f, 1.0f, 0.0f,
            0.5f, -0.5f, -0.5f,  0.0f, 1.0f, 0.0f,
            0.5f, -0.5f,  0.5f,  0.0f, 0.0f, 0.0f,
            0.5f,  0.5f,  0.5f,  1.0f, 0.0f, 0.0f,

            -0.5f, -0.5f, -0.5f,  0.0f, 1.0f, 0.0f,
            0.5f, -0.5f, -0.5f,  1.0f, 1.0f, 0.0f,
            0.5f, -0.5f,  0.5f,  1.0f, 0.0f, 0.0f,
            0.5f, -0.5f,  0.5f,  1.0f, 0.0f, 0.0f,
            -0.5f, -0.5f,  0.5f,  0.0f, 0.0f, 0.0f,
            -0.5f, -0.5f, -0.5f,  0.0f, 1.0f, 0.0f,

            -0.5f,  0.5f, -0.5f,  0.0f, 1.0f, 0.0f,
            0.5f,  0.5f, -0.5f,  1.0f, 1.0f, 0.0f,
            0.5f,  0.5f,  0.5f,  1.0f, 0.0f, 0.0f,
            0.5f,  0.5f,  0.5f,  1.0f, 0.0f, 0.0f,
            -0.5f,  0.5f,  0.5f,  0.0f, 0.0f, 0.0f,
            -0.5f,  0.5f, -0.5f,  0.0f, 1.0f, 0.0f
    });

    Vector3f cubePositions[] = new Vector3f[]{
        new Vector3f( 0.0f,  0.0f,  0.0f),
        new Vector3f( 2.0f,  5.0f, -15.0f),
        new Vector3f(-1.5f, -2.2f, -2.5f),
        new Vector3f(-3.8f, -2.0f, -12.3f),
        new Vector3f( 2.4f, -0.4f, -3.5f),
        new Vector3f(-1.7f,  3.0f, -7.5f),
        new Vector3f( 1.3f, -2.0f, -2.5f),
        new Vector3f( 1.5f,  2.0f, -2.5f),
        new Vector3f( 1.5f,  0.2f, -1.5f),
        new Vector3f(-1.3f,  1.0f, -1.5f)
    };

    int vaos[] = new int[1];
    int vbos[] = new int[1];
    ShaderProgram program;

    FloatBuffer fb = Buffers.newDirectFloatBuffer(16);
    Matrix4f model = new Matrix4f();
    Matrix4f view = new Matrix4f();
    Matrix4f proj = new Matrix4f();


    @Override
    public void init(GL4 gl) {
        program = new ShaderProgram(gl,"/learnopengl_04_coords/shader/v_01.vert"
                ,"/learnopengl_04_coords/shader/f_01.frag");
        program.addUniform(gl,"u_model");
        program.addUniform(gl,"u_view");
        program.addUniform(gl,"u_proj");

        view.translate(0f,0f,-5f);
        proj.perspective(45.0f,800.0f/600.0f,0.1f,100f);

        gl.glGenVertexArrays(1, vaos, 0);
        gl.glGenBuffers(1, vbos, 0);

        gl.glBindVertexArray(vaos[0]);
        {
            gl.glBindBuffer(GL4.GL_ARRAY_BUFFER, vbos[0]);
            gl.glBufferData(GL4.GL_ARRAY_BUFFER, vertices.capacity()*Buffers.SIZEOF_FLOAT,vertices,GL4.GL_STATIC_DRAW);

            gl.glVertexAttribPointer(0,3,GL4.GL_FLOAT,false,Buffers.SIZEOF_FLOAT*6,0);
            gl.glEnableVertexAttribArray(0);

            gl.glVertexAttribPointer(1,3,GL4.GL_FLOAT,false,Buffers.SIZEOF_FLOAT*6,3*4);
            gl.glEnableVertexAttribArray(1);
        }
        gl.glBindVertexArray(0);

        //gl.glDeleteBuffers(1,vbos,0);
    }

    @Override
    public void update() {

    }

    @Override
    public void render(GL4 gl, float inter, float time, long count) {
        program.use(gl);

        view.translate(0f,0f,(float)Math.sin(count*0.01));

        gl.glUniformMatrix4fv(program.getUniform("u_view"),1,false,view.get(fb));
        gl.glUniformMatrix4fv(program.getUniform("u_proj"),1,false,proj.get(fb));

        gl.glBindVertexArray(vaos[0]);

        for(int i = 0 ; i < cubePositions.length; i++){
            Vector3f v = cubePositions[i];
            model.identity();
            model.setRotationXYZ(v.x*count*0.001f,v.y*count*0.001f,v.z*count*0.001f);
            model.setTranslation(v.x,v.y,v.z);
            gl.glUniformMatrix4fv(program.getUniform("u_model"),1,false,model.get(fb));
            gl.glDrawArrays(GL4.GL_TRIANGLES, 0, 36);
        }

        gl.glBindVertexArray(0);
    }

    @Override
    public void dispose(GL4 gl) {
        gl.glDeleteVertexArrays(1,vaos,0);
    }

    @Override
    public void reshape(GL4 gl, int x, int y, int width, int height) {

    }

    public static void main(String[] args){
        coords_03 engine = new coords_03();
        engine.setTitle("coords_03");
        JOGLWrapper wrapper = new JOGLWrapper(engine);
    }
}
