package nz.minigen.loader;

import nz.minigen.log.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ballas on 1/14/2016.
 */
public class TextLoader {

    public static String load(String src){
        InputStream in = TextLoader.class.getResourceAsStream(src);
        if(in == null){
            StackTraceElement[] trace = Thread.currentThread().getStackTrace();
            Log.e("Could not load text file: "+src);
            Log.e("Returned Empty String!");
            Log.e("Stack Trace:");
            for(int i = 0; i < trace.length && i < 3; i++){
                Log.e("\t\t"+trace[i]);
            }
            return "";
        }
        BufferedReader input = new BufferedReader(new InputStreamReader(in));
        String line;
        try {
            StringBuilder result = new StringBuilder();
            while((line = input.readLine()) != null){
                result.append(line+"\n");
            }
            return result.toString();
        } catch (IOException e) {
            e.printStackTrace();
        }

        StackTraceElement[] trace = Thread.currentThread().getStackTrace();
        Log.e("Could not load text file: "+src);
        Log.e("Returned Empty String!");
        Log.e("Stack Trace:");
        for(int i = 0; i < trace.length && i < 3; i++){
            Log.e("\t\t"+trace[i]);
        }
        return "";
    }

}
