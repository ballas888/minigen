package test_0_0_2_gridTest;

import com.jogamp.opengl.GL4;
import nz.minigen.AbstractCallback;
import nz.minigen.JOGLWrapper;
import nz.minigen.mesh.Mesh;
import nz.minigen.shader.ShaderProgram;
import org.joml.Matrix4f;

import java.nio.FloatBuffer;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by Richard on 13/03/2016.
 */
public class test_0_0_2_gridTest extends AbstractCallback {

    int vertexArrayId;

    Matrix4f projection = new Matrix4f();
    Matrix4f view = new Matrix4f();
    Matrix4f model = new Matrix4f();
    Matrix4f pvMatrix = new Matrix4f();

    @Override
    public void init(GL4 gl) {
        int vao[] = new int[1];

        gl.glGenVertexArrays(1, vao, 0);
        vertexArrayId = vao[0];

        gl.glBindVertexArray(vertexArrayId);

        float[] vertices = {
                -1.0f, -1.0f, 0.0f,
                1.0f, -1.0f, 0.0f,
                0.0f, 1.0f, 0.0f
        };

        FloatBuffer vertBuffer = FloatBuffer.allocate(vertices.length);
        vertBuffer.put(vertices).flip();

        int[] vbos = new int[1];
        gl.glGenBuffers(1,vbos,0);
        int vboId = vbos[0];

        gl.glBindBuffer(GL4.GL_ARRAY_BUFFER,vboId);
        gl.glBufferData(GL4.GL_ARRAY_BUFFER,vertices.length*4,vertBuffer,GL4.GL_STATIC_DRAW);

        gl.glVertexAttribPointer(0,3,GL4.GL_FLOAT,false,0,0);

        gl.glEnableVertexAttribArray(0);


        gl.glBindVertexArray(0);
        gl.glDisableVertexAttribArray(0);

        ShaderProgram program = new ShaderProgram(gl, "/test_0_0_2_gridTest/ch_01.vert","/test_0_0_2_gridTest/ch_01.frag");
        program.use(gl);

        //projection.setOrtho(0,getCanvasWidth(),getCanvasHeight(),0,0.1f,1000.0f);
        projection.setPerspective(45,getCanvasWidth()/getCanvasHeight(),0.1f,1000f);
        model.translate(0,0,-0.5f);

        //gl.glGetUniformLocation()

    }

    @Override
    public void update() {

    }

    @Override
    public void render(GL4 gl, float inter, float time, long count) {
        gl.glBindVertexArray(vertexArrayId);
        projection.mul(view,pvMatrix);
        gl.glUniformMatrix4fv(1,1,false,pvMatrix.get(new float[16]),0);
        gl.glUniformMatrix4fv(2,1,false,model.get(new float[16]),0);

        gl.glDrawArrays(GL4.GL_TRIANGLES, 0 ,3);

        gl.glBindVertexArray(0);
    }

    @Override
    public void dispose(GL4 gl) {
        gl.glBindVertexArray(0);
        gl.glDeleteVertexArrays(1, new int[]{vertexArrayId}, 0);
    }

    @Override
    public void reshape(GL4 gl, int x, int y, int width, int height) {


    }

    public static void main(String[] args){
        test_0_0_2_gridTest engine = new test_0_0_2_gridTest();
        engine.setTitle("test_0_0_2_gridTest.test_0_0_2_gridTest.java");
        JOGLWrapper wrapper = new JOGLWrapper(engine);
    }
}
