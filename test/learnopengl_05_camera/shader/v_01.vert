#version 450 core

layout (location = 0) in vec3 in_position;
layout (location = 1) in vec3 in_color;

uniform mat4 u_model;
uniform mat4 u_view;
uniform mat4 u_proj;

out vec4 vs_color;
void main(void)
{
    gl_Position = u_proj*u_view*u_model*vec4(in_position, 1.0f);
    vs_color = vec4(in_color, 1.0f);
}