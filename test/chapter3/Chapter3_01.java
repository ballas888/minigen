package chapter3;

import com.jogamp.opengl.GL4;
import nz.minigen.AbstractCallback;
import nz.minigen.JOGLWrapper;
import nz.minigen.shader.ShaderProgram;

import java.nio.IntBuffer;

/**
 * Created by Ballas on 1/14/2016.
 */
public class Chapter3_01 extends AbstractCallback {

    int vertexArray;
    ShaderProgram program;
    float[] attrib = {0.0f,0.0f,0.0f,0.0f};

    float posx = 0.0f;
    float posy = 0.0f;

    float diffx = 0.0f;
    float diffy = 0.0f;

    float oldx = 0.0f;
    float oldy = 0.0f;

    @Override
    public void init(GL4 gl) {
        program = new ShaderProgram(gl, "/chapter3/ch_01.vert","/chapter3/ch_01.frag");
        int[] vao = new int[1];
        gl.glCreateVertexArrays(1,vao,0);
        vertexArray = vao[0];
        gl.glBindVertexArray(vertexArray);


    }

    long c = 0;
    @Override
    public void update() {
        oldx = posx;
        oldy = posy;
        posx = (float)Math.sin(c*0.11)*0.5f;
        posy = (float)Math.cos(c*0.11)*0.5f;

        diffx = posx - oldx;
        diffy = posy - oldy;
        System.out.println(getCanvasWidth()+" "+getCanvasHeight());

        c++;
    }

    @Override
    public void render(GL4 gl, float inter, float time, long count) {

        program.use(gl);
//        float[] attrib = {  (float)Math.sin(time*0.001) * 0.5f,
//                            (float)Math.cos(time*0.001) * 0.6f,
//                            0.0f, 0.0f };
        attrib[0] = oldx+(diffx*inter);//(diffx*inter);//oldx + (diffx*inter);
        attrib[1] = oldy+(diffy*inter);//(diffy*inter);//oldy + (diffy*inter);
        gl.glVertexAttrib4fv(0,attrib,0);
        gl.glVertexAttrib4fv(1,attrib,0);
        gl.glDrawArrays(GL4.GL_TRIANGLES,0,3);
    }

    @Override
    public void dispose(GL4 gl) {
        gl.glDeleteVertexArrays(1,new int[]{vertexArray},0);
        program.dispose(gl);
    }

    @Override
    public void reshape(GL4 gl, int x, int y, int width, int height) {

    }

    public static void main(String[] args){
        Chapter3_01 engine = new Chapter3_01();
        engine.setTitle("Chapter3.Chapter3_01");
        JOGLWrapper wrapper = new JOGLWrapper(engine);
    }
}
