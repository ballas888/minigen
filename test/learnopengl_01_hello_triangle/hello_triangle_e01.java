package learnopengl_01_hello_triangle;

import com.jogamp.opengl.GL4;
import nz.minigen.AbstractCallback;
import nz.minigen.JOGLWrapper;
import nz.minigen.shader.ShaderProgram;

import java.nio.Buffer;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;

/**
 * Created by Richard on 13/03/2016.
 */
public class hello_triangle_e01 extends AbstractCallback {

    float vertices[] = {
        // First triangle
        -0.9f, -0.5f, 0.0f,  // Left
        -0.0f, -0.5f, 0.0f,  // Right
        -0.45f, 0.5f, 0.0f,  // Top
        // Second triangle
        0.0f, -0.5f, 0.0f,  // Left
        0.9f, -0.5f, 0.0f,  // Right
        0.45f, 0.5f, 0.0f   // Top
    };

    int indices[] = {
        0, 1, 3,   // First Triangle
        1, 2, 3    // Second Triangle
    };

    Buffer vertBuffer = FloatBuffer.allocate(vertices.length).put(vertices).flip();
    Buffer indeBuffer = IntBuffer.allocate(indices.length).put(indices).flip();

    int vbo;
    int vao;
    int ebo;
    ShaderProgram program;


    @Override
    public void init(GL4 gl) {
        program = new ShaderProgram(gl, "/learnopengl_01_hello_triangle/shader/v_01.vert", "/learnopengl_01_hello_triangle/shader/f_01.frag");

        int vaoArray[] = new int[1];
        gl.glGenVertexArrays(1,vaoArray,0);
        vao = vaoArray[0];

        int vboArray[] = new int[1];
        gl.glGenBuffers(1,vboArray,0);
        vbo = vboArray[0];

//        int eboArray[] = new int[1];
//        gl.glGenBuffers(1,eboArray,0);
//        ebo = eboArray[0];

        gl.glBindVertexArray(vao);

        gl.glBindBuffer(GL4.GL_ARRAY_BUFFER, vbo);
        gl.glBufferData(GL4.GL_ARRAY_BUFFER, 4* vertices.length*3,vertBuffer,GL4.GL_STATIC_DRAW);

        //gl.glBindBuffer(GL4.GL_ELEMENT_ARRAY_BUFFER, ebo);
        //gl.glBufferData(GL4.GL_ELEMENT_ARRAY_BUFFER, indices.length*4, indeBuffer, GL4.GL_STATIC_DRAW);

        gl.glVertexAttribPointer(0,3,GL4.GL_FLOAT,false,0,0);
        gl.glEnableVertexAttribArray(0);

        gl.glBindVertexArray(0);

        //gl.glPolygonMode(GL4.GL_FRONT_AND_BACK, GL4.GL_LINE);
    }

    @Override
    public void update() {

    }

    @Override
    public void render(GL4 gl, float inter, float time, long count) {
        program.use(gl);

        gl.glBindVertexArray(vao);
        //gl.glDrawElements(GL4.GL_TRIANGLES, 6, GL4.GL_UNSIGNED_INT,0);
        gl.glDrawArrays(GL4.GL_TRIANGLES, 0, 6);
        gl.glBindVertexArray(0);
    }

    @Override
    public void dispose(GL4 gl) {

    }

    @Override
    public void reshape(GL4 gl, int x, int y, int width, int height) {

    }

    public static void main(String[] args){
        hello_triangle_e01 engine = new hello_triangle_e01();
        engine.setTitle("hello_triangle.hello_triangle_02");
        JOGLWrapper wrapper = new JOGLWrapper(engine);
    }
}
