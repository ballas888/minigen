package nz.minigen.log;

/**
 * Created by Ballas on 1/4/2016.
 */
public class Log {
    public static void i(Object obj){
        System.out.println(getStackTrace()+obj);
    }

    public static void e(Object obj){
        System.err.println(getStackTrace()+obj);
    }

    public static String getStackTrace(){
        String fullClassName = Thread.currentThread().getStackTrace()[3].getClassName();
        String className = fullClassName.substring(fullClassName.lastIndexOf(".") + 1);
        int lineNumber = Thread.currentThread().getStackTrace()[3].getLineNumber();
        return className+":"+lineNumber+"> ";
    }
}
