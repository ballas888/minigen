import com.jogamp.opengl.GL4;
import nz.minigen.AbstractCallback;
import nz.minigen.JOGLWrapper;

public class TestEngine01 extends AbstractCallback{
    @Override
    public void init(GL4 gl) {

    }

    @Override
    public void update() {

    }

    @Override
    public void render(GL4 gl, float inter, float time, long count) {

    }

    @Override
    public void dispose(GL4 gl) {

    }

    @Override
    public void reshape(GL4 gl, int x, int y, int width, int height) {

    }

    public static void main(String[] args){
        TestEngine01 engine = new TestEngine01();
        engine.setTitle("TestEngine01");
        JOGLWrapper wrapper = new JOGLWrapper(engine);
    }
}
