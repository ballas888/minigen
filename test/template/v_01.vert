#version 450 core

layout (location = 0) in vec4 in_position;
layout (location = 1) in vec4 in_color;

out vec4 vs_color;
void main(void)
{
    gl_Position = in_position;
    vs_color = in_color;
}