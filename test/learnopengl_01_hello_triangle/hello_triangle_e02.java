package learnopengl_01_hello_triangle;

import com.jogamp.opengl.GL4;
import nz.minigen.AbstractCallback;
import nz.minigen.JOGLWrapper;
import nz.minigen.shader.ShaderProgram;

import java.nio.Buffer;
import java.nio.FloatBuffer;

/**
 * Created by Richard on 13/03/2016.
 */
public class hello_triangle_e02 extends AbstractCallback {

    float firstTriangleData[] = {
        -0.9f, -0.5f, 0.0f,  // Left
        -0.0f, -0.5f, 0.0f,  // Right
        -0.45f, 0.5f, 0.0f,  // Top
    };

    float secondTriangleData[] = {
        0.0f, -0.5f, 0.0f,  // Left
        0.9f, -0.5f, 0.0f,  // Right
        0.45f, 0.5f, 0.0f   // Top
    };

    Buffer firstTriangle = FloatBuffer.allocate(firstTriangleData.length).put(firstTriangleData).flip();
    Buffer secondTriangle = FloatBuffer.allocate(secondTriangleData.length).put(secondTriangleData).flip();

    int vbos[] = new int[2];
    int vaos[] = new int[2];

    ShaderProgram program;

    @Override
    public void init(GL4 gl) {
        program = new ShaderProgram(gl, "/learnopengl_01_hello_triangle/shader/v_01.vert", "/learnopengl_01_hello_triangle/shader/f_01.frag");

        gl.glGenVertexArrays(2, vaos,0);
        gl.glGenBuffers(2, vbos, 0);

        //First Triangle
        gl.glBindVertexArray(vaos[0]);
        gl.glBindBuffer(GL4.GL_ARRAY_BUFFER, vbos[0]);
        gl.glBufferData(GL4.GL_ARRAY_BUFFER, firstTriangleData.length*4*3, firstTriangle, GL4.GL_STATIC_DRAW);
        gl.glVertexAttribPointer(0, 3, GL4.GL_FLOAT, false, 0,0);
        gl.glEnableVertexAttribArray(0);
        gl.glBindVertexArray(0);

        //Second Triangle
        gl.glBindVertexArray(vaos[1]);
        gl.glBindBuffer(GL4.GL_ARRAY_BUFFER, vbos[1]);
        gl.glBufferData(GL4.GL_ARRAY_BUFFER, secondTriangleData.length*4*3, secondTriangle, GL4.GL_STATIC_DRAW);
        gl.glVertexAttribPointer(0, 3, GL4.GL_FLOAT, false, 0,0);
        gl.glEnableVertexAttribArray(0);
        gl.glBindVertexArray(0);
    }

    @Override
    public void update() {

    }

    @Override
    public void render(GL4 gl, float inter, float time, long count) {
        program.use(gl);

        gl.glBindVertexArray(vaos[0]);
        gl.glDrawArrays(GL4.GL_TRIANGLES,0,3);

        gl.glBindVertexArray(vaos[1]);
        gl.glDrawArrays(GL4.GL_TRIANGLES,0,3);

        gl.glBindVertexArray(0);
    }

    @Override
    public void dispose(GL4 gl) {

    }

    @Override
    public void reshape(GL4 gl, int x, int y, int width, int height) {

    }

    public static void main(String[] args){
        hello_triangle_e02 engine = new hello_triangle_e02();
        engine.setTitle("hello_triangle.hello_triangle_02");
        JOGLWrapper wrapper = new JOGLWrapper(engine);
    }
}
