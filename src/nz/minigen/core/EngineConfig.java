package nz.minigen.core;

/**
 * Created by Richard on 27/03/2016.
 */
public class EngineConfig {
    public int WIDTH = 800;
    public int HEIGHT = 600;
    public float UPDATE_PER_SECOND = 30.0f;
    public int UPDATE_MAX_FRAME_SKIP = 1;
    public float RENDER_PER_SECOND = 61.0f;
    public int RENDER_MAX_FRAME_SKIP = 1;
    public boolean VSYNC = true;
    public boolean DEBUG = true;
    public boolean TRACE = false;
}
