package test_scene;

import com.jogamp.opengl.GL3;
import nz.minigen.CorePackage;

/**
 * Created by Richard on 27/03/2016.
 */
public class test_core extends CorePackage {

    @Override
    public void setup(GL3 gl) {
        //addScene("test_scene",new test_scene());
        addScene("test_scene",new test_scene());
        addScene("test_scene_01",new test_scene_02());
        setCurrentScene("test_scene_01");


    }

    @Override
    public void checker() {

    }

    public static void main(String[] args){
        test_core core = new test_core();
    }
}
