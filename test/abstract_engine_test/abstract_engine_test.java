package abstract_engine_test;

import com.jogamp.common.nio.Buffers;
import com.jogamp.opengl.GL3;
import com.jogamp.opengl.GL4;
import nz.minigen.camera.Camera;
import nz.minigen.core.AbstractEngineOld;
import nz.minigen.shader.ShaderProgram;
import org.joml.Matrix4f;

import java.nio.FloatBuffer;

/**
 * Created by Richard on 27/03/2016.
 */
public class abstract_engine_test extends AbstractEngineOld {

    private final float PI = (float)Math.PI;

    FloatBuffer vertices = Buffers.newDirectFloatBuffer(new float[]{
            -0.5f, -0.5f, -0.5f,  0.0f, 0.0f, 0.0f,
            0.5f, -0.5f, -0.5f,  1.0f, 0.0f, 0.0f,
            0.5f,  0.5f, -0.5f,  1.0f, 1.0f, 0.0f,
            0.5f,  0.5f, -0.5f,  1.0f, 1.0f, 0.0f,
            -0.5f,  0.5f, -0.5f,  0.0f, 1.0f, 0.0f,
            -0.5f, -0.5f, -0.5f,  0.0f, 0.0f, 0.0f,

            -0.5f, -0.5f,  0.5f,  0.0f, 0.0f, 0.0f,
            0.5f, -0.5f,  0.5f,  1.0f, 0.0f, 0.0f,
            0.5f,  0.5f,  0.5f,  1.0f, 1.0f, 0.0f,
            0.5f,  0.5f,  0.5f,  1.0f, 1.0f, 0.0f,
            -0.5f,  0.5f,  0.5f,  0.0f, 1.0f, 0.0f,
            -0.5f, -0.5f,  0.5f,  0.0f, 0.0f, 0.0f,

            -0.5f,  0.5f,  0.5f,  1.0f, 0.0f, 0.0f,
            -0.5f,  0.5f, -0.5f,  1.0f, 1.0f, 0.0f,
            -0.5f, -0.5f, -0.5f,  0.0f, 1.0f, 0.0f,
            -0.5f, -0.5f, -0.5f,  0.0f, 1.0f, 0.0f,
            -0.5f, -0.5f,  0.5f,  0.0f, 0.0f, 0.0f,
            -0.5f,  0.5f,  0.5f,  1.0f, 0.0f, 0.0f,

            0.5f,  0.5f,  0.5f,  1.0f, 0.0f, 0.0f,
            0.5f,  0.5f, -0.5f,  1.0f, 1.0f, 0.0f,
            0.5f, -0.5f, -0.5f,  0.0f, 1.0f, 0.0f,
            0.5f, -0.5f, -0.5f,  0.0f, 1.0f, 0.0f,
            0.5f, -0.5f,  0.5f,  0.0f, 0.0f, 0.0f,
            0.5f,  0.5f,  0.5f,  1.0f, 0.0f, 0.0f,

            -0.5f, -0.5f, -0.5f,  0.0f, 1.0f, 0.0f,
            0.5f, -0.5f, -0.5f,  1.0f, 1.0f, 0.0f,
            0.5f, -0.5f,  0.5f,  1.0f, 0.0f, 0.0f,
            0.5f, -0.5f,  0.5f,  1.0f, 0.0f, 0.0f,
            -0.5f, -0.5f,  0.5f,  0.0f, 0.0f, 0.0f,
            -0.5f, -0.5f, -0.5f,  0.0f, 1.0f, 0.0f,

            -0.5f,  0.5f, -0.5f,  0.0f, 1.0f, 0.0f,
            0.5f,  0.5f, -0.5f,  1.0f, 1.0f, 0.0f,
            0.5f,  0.5f,  0.5f,  1.0f, 0.0f, 0.0f,
            0.5f,  0.5f,  0.5f,  1.0f, 0.0f, 0.0f,
            -0.5f,  0.5f,  0.5f,  0.0f, 0.0f, 0.0f,
            -0.5f,  0.5f, -0.5f,  0.0f, 1.0f, 0.0f
    });

    int vaos[] = new int[1];
    ShaderProgram program;

    FloatBuffer fb = Buffers.newDirectFloatBuffer(16);
    Matrix4f model = new Matrix4f();
    //Matrix4f view = new Matrix4f();
    Matrix4f proj = new Matrix4f();

    Camera camera = new Camera();
    
    
    @Override
    public void initialize(GL3 gl) {
        program = new ShaderProgram(gl,"/learnopengl_04_coords/shader/v_01.vert"
                ,"/learnopengl_04_coords/shader/f_01.frag");
        program.addUniform(gl,"u_model");
        program.addUniform(gl,"u_view");
        program.addUniform(gl,"u_proj");


        camera.translate(0f,0f,-3f);
        proj.perspective(45.0f,800.0f/600.0f,0.1f,100f);

        int vbos[] = new int[1];

        gl.glGenVertexArrays(1, vaos, 0);
        gl.glGenBuffers(1, vbos, 0);

        gl.glBindVertexArray(vaos[0]);
        {
            gl.glBindBuffer(GL4.GL_ARRAY_BUFFER, vbos[0]);
            gl.glBufferData(GL4.GL_ARRAY_BUFFER, vertices.capacity()*Buffers.SIZEOF_FLOAT,vertices,GL4.GL_STATIC_DRAW);

            gl.glVertexAttribPointer(0,3,GL4.GL_FLOAT,false,Buffers.SIZEOF_FLOAT*6,0);
            gl.glEnableVertexAttribArray(0);

            gl.glVertexAttribPointer(1,3,GL4.GL_FLOAT,false,Buffers.SIZEOF_FLOAT*6,3*4);
            gl.glEnableVertexAttribArray(1);
        }
        gl.glBindVertexArray(0);
    }

    @Override
    public void logicLoop(GL3 gl, float time, long count) {
        float step = 0.1f;
        if(keyboard.keyUp){
            camera.translate(0,0,step);
        }
        if(keyboard.keyDown){
            camera.translate(0,0,-step);
        }
        if(keyboard.keyLeft){
            camera.translate(step,0f,0);
        }
        if(keyboard.keyRight){
            camera.translate(-step,0,0);
        }
        camera.update();
        //System.out.println("UPDATE");
    }

    @Override
    public void renderLoop(GL3 gl, float inter, float time, long count) {
        program.use(gl);
        //System.out.println(inter);
        model.rotate(0.01f,0.5f,1.0f,0f);

        gl.glUniformMatrix4fv(program.getUniform("u_model"),1,false,model.get(fb));
        gl.glUniformMatrix4fv(program.getUniform("u_view"),1,false,camera.getView(inter));
        gl.glUniformMatrix4fv(program.getUniform("u_proj"),1,false,proj.get(fb));

        gl.glBindVertexArray(vaos[0]);
        //gl.glDrawElements(GL4.GL_TRIANGLES, indices.capacity(), GL4.GL_UNSIGNED_INT,0);
        gl.glDrawArrays(GL4.GL_TRIANGLES, 0, 36);
        gl.glBindVertexArray(0);
    }

    @Override
    public void resize(GL3 gl, int x, int y, int width, int height) {

    }


    @Override
    public void terminate(GL3 gl) {

    }

    public static void main(String[] args){
        abstract_engine_test e = new abstract_engine_test();
        e.start();
    }
}
