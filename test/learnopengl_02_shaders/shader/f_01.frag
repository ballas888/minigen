#version 450 core

in vec3 out_color;

uniform vec3 u_color;

out vec4 color;

void main(void)
{
    color = vec4(out_color*u_color,1.0);
}
