package abstract_engine_test;

import org.joml.Vector3f;

/**
 * Created by Richard on 28/03/2016.
 */
public class test_vector {

    public static void main(String[] args){
        Vector3f v = new Vector3f(2,3,4);
        Vector3f d = new Vector3f(3,3,3);

        d.x = v.x;
        d.y = v.y;
        d.z = v.z;

        v.add(2,2,2);

        System.out.println(v);
        System.out.println(d);
    }
}
