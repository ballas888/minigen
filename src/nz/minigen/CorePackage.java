package nz.minigen;

import com.jogamp.opengl.*;
import com.jogamp.opengl.awt.GLCanvas;
import com.jogamp.opengl.util.glsl.ShaderUtil;
import nz.minigen.input.Keyboard;
import nz.minigen.log.GLTraceLog;

import javax.swing.*;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Richard on 26-Mar-16.
 */
public abstract class CorePackage extends JFrame implements GLEventListener{

    private float updateTicksPerSecond;// = 25.0f;
    private float updateSkipTicks;// = 1000.0f/updateTicksPerSecond;
    private double updateNextTick = 0.0f;
    private int updateMaxFrameSkip = 5;
    private int updateLoops = 0;
    private int updateCount = 0;

    private float renderTicksPerSecond;// = 62.0f;
    private float renderSkipTicks;// = 1000.0f/renderTicksPerSecond;
    private double renderNextTick = 0.0f;
    private int renderMaxFrameSkip = 5;
    private int renderLoops = 0;
    private int renderCount = 0;
    private float interpolation = 0.0f;
    private long count = 0;

    private long startTime = 0;
    private boolean isRunning = false;

    private String appTitle = "minigen";

    private WrapperConfig config;
    private GLCanvas glcanvas;
    protected Keyboard keyboard;
    private GL3 curGL;

    private Map<String,Scene> sceneCollection = new HashMap<>();
    private Scene currentScene = null;

    public CorePackage(){
        this(new WrapperConfig());
    }

    public CorePackage(WrapperConfig config){
        this.config = config;
        this.keyboard = new Keyboard();

        updateTicksPerSecond = config.updatePerSecond;
        updateSkipTicks = 1000.0f/updateTicksPerSecond;

        renderTicksPerSecond = config.renderPerSecond;
        renderSkipTicks = 1000.0f/renderTicksPerSecond;


        GLProfile profile = GLProfile.get(GLProfile.GL3);
        GLCapabilities capabilities = new GLCapabilities(profile);
        glcanvas = new GLCanvas(capabilities);
        glcanvas.addGLEventListener(this);
        glcanvas.addKeyListener(this.keyboard);

        getContentPane().add(glcanvas);

        setSize(config.width, config.height);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setVisible(true);
        setResizable(false);
        glcanvas.requestFocusInWindow();

    }

    public abstract void setup(GL3 gl);
    public abstract void checker();

    @Override
    public void init(GLAutoDrawable glAutoDrawable) {
        GL drawable = glAutoDrawable.getGL().getGL3();

        if(config.debug){
            try {
                // Debug ..
                drawable = drawable.getContext().setGL( GLPipelineFactory.create("com.jogamp.opengl.Debug", null, drawable, null) );
            } catch (Exception e) {e.printStackTrace();}
        }

        if(config.trace) {
            try {
                // Trace ..
                drawable = drawable.getContext().setGL( GLPipelineFactory.create("com.jogamp.opengl.Trace", null, drawable, new Object[] { new GLTraceLog(System.err)} ) );
            } catch (Exception e) {e.printStackTrace();}
        }

        GL3 gl = drawable.getGL3();
        //GL setting
        gl.setSwapInterval(config.vsync?1:0);
        gl.glClearColor(0.392f, 0.584f, 0.929f, 1.0f);
        gl.glEnable(GL3.GL_DEPTH_TEST);

        System.out.println("------------------------------------------------------------------");
        System.out.println("Entering Initialization!");
        System.out.println("GL Profile: "+ gl.getGLProfile());
        System.out.println("GL: "+gl);
        System.out.println("GL Version: "+gl.glGetString(GL.GL_VERSION));
        System.out.println("Vsync: "+gl.getSwapInterval());
        System.out.println("isShaderCompilerAvailible: "+ ShaderUtil.isShaderCompilerAvailable(gl));
        System.out.println("Program Ready!");
        System.out.println("------------------------------------------------------------------\n");


        curGL = gl;
        setup(gl);

//        Thread loop = new Thread(new Runnable() {
//            @Override
//            public void run() {
//                loop();
//            }
//        });
//        loop.setName("Loop Thread");
//        loop.start();
        loop();

    }

    private void loop(){
        isRunning = true;
        updateNextTick = time();
        renderNextTick = time();
        double oldTime = time();

        while(isRunning){
            updateLoops = 0;
            renderLoops = 0;
            System.out.println("Loop");

            keyboard.update();
            if(keyboard.keyNum1){
                setCurrentScene("test_scene");
            }else if(keyboard.keyNum2){
                setCurrentScene("test_scene_01");
            }

            while(time() > updateNextTick && updateLoops < updateMaxFrameSkip){
                update();
                updateNextTick+=updateSkipTicks;
                updateLoops++;
            }

            while(time() > renderNextTick && renderLoops < renderMaxFrameSkip){
                glcanvas.repaint();
                renderNextTick+=renderSkipTicks;
                renderLoops++;
            }

            if(time() - oldTime > 1000.0){
                //System.out.println("FPS: "+renderCount+" UPS: "+updateCount);
                setTitle(this.appTitle+"     FPS: "+renderCount+" UPS: "+updateCount);
                updateCount = 0;
                renderCount = 0;
                oldTime += 1000.0;
            }

        }
    }

    public void update(){
        //callback.update();
        if(currentScene != null){
            currentScene.update();
        }
        checker();
        updateCount++;
    }

    @Override
    public void dispose(GLAutoDrawable glAutoDrawable) {
        isRunning = false;
        GL3 gl = glAutoDrawable.getGL().getGL3();
        if(currentScene != null){
            currentScene.dispose(gl);
        }
        //callback.dispose(gl);
    }

    @Override
    public void display(GLAutoDrawable glAutoDrawable) {
        GL3 gl = glAutoDrawable.getGL().getGL3();
        //gl.glClearBufferfv(GL4.GL_COLOR,0,clearColor,0);
        gl.glClear(GL3.GL_COLOR_BUFFER_BIT|GL3.GL_DEPTH_BUFFER_BIT);


        interpolation = (float)((time()+updateSkipTicks-updateNextTick)/updateSkipTicks);
        //callback.render(gl,interpolation, (float)time(), count);
        if(currentScene != null){
            currentScene.render(gl, interpolation, (float)time(), count);
        }
        count++;

        renderCount++;
    }

    @Override
    public void reshape(GLAutoDrawable glAutoDrawable, int x, int y, int width, int height) {
        GL3 gl = glAutoDrawable.getGL().getGL3();
        gl.glViewport(x,y,width,height);
        //callback.setCanvasWidth(width);
        //callback.setCanvasHeight(height);
        //callback.reshape(gl,x,y,width,height);
        if(currentScene != null){
            currentScene.reshape(gl,x,y,width,height);
        }
    }

    public double time(){
        if(startTime == 0.0){
            startTime = System.nanoTime();
            return 0.0f;
        }
        long currentTime = System.nanoTime();
        return (currentTime - startTime)/1000000.0;
    }

    public void addScene(String sceneName, Scene scene){
        scene.setSceneName(sceneName);
        sceneCollection.put(scene.getSceneName(),scene);
    }

    public void removeScene(String name){
        sceneCollection.remove(name);
    }

    public void setCurrentScene(String name){
        Scene scene = sceneCollection.get(name);
        if(!scene.isInit()){
            scene.init(curGL);
        }
        this.currentScene = sceneCollection.get(name);
    }

    public void setCurrentScene(Scene scene){
        this.currentScene =scene;
    }
}
