package learnopengl_03_transform;

import com.jogamp.common.nio.Buffers;
import com.jogamp.opengl.GL4;
import nz.minigen.AbstractCallback;
import nz.minigen.JOGLWrapper;
import nz.minigen.shader.ShaderProgram;
import org.joml.Matrix4f;

import java.nio.FloatBuffer;

/**
 * Created by Richard on 25-Mar-16.
 */
public class transform_01 extends AbstractCallback {

    int vbos[] = new int[1];
    int vaos[] = new int[1];
    ShaderProgram program;

    Matrix4f matrix = new Matrix4f();
    FloatBuffer fb = Buffers.newDirectFloatBuffer(16);

    @Override
    public void init(GL4 gl) {

        FloatBuffer vertices = Buffers.newDirectFloatBuffer(new float[]{
            0.5f, -0.5f, 0.0f,  1.0f, 0.0f, 0.0f,  // Bottom Right
            -0.5f, -0.5f, 0.0f,  0.0f, 1.0f, 0.0f,  // Bottom Left
            0.0f,  0.5f, 0.0f,  0.0f, 0.0f, 1.0f
        });

        program = new ShaderProgram(gl, "/learnopengl_03_transform/shader/v_01.vert", "/learnopengl_03_transform/shader/f_01.frag");
        program.addUniform(gl,"u_matrix");

        gl.glGenVertexArrays(1,vaos, 0);
        gl.glGenBuffers(1, vbos, 0);

        gl.glBindVertexArray(vaos[0]);
        {
            gl.glBindBuffer(GL4.GL_ARRAY_BUFFER, vbos[0]);
            gl.glBufferData(GL4.GL_ARRAY_BUFFER,vertices.capacity()*Buffers.SIZEOF_FLOAT,vertices,GL4.GL_STATIC_DRAW);

            gl.glVertexAttribPointer(0,3,GL4.GL_FLOAT,false,Buffers.SIZEOF_FLOAT*6,0);
            gl.glEnableVertexAttribArray(0);

            gl.glVertexAttribPointer(1,3,GL4.GL_FLOAT,false,Buffers.SIZEOF_FLOAT*6,3*4);
            gl.glEnableVertexAttribArray(1);
        }
        gl.glBindVertexArray(0);

        matrix.rotate((float)Math.PI/2.0f,0f,0f,1f);
        matrix.scale(0.5f,0.5f,0.5f);

    }

    @Override
    public void update() {

    }

    @Override
    public void render(GL4 gl, float inter, float time, long count) {
        program.use(gl);
        gl.glUniformMatrix4fv(program.getUniform("u_matrix"),1,false,matrix.get(fb));

        gl.glBindVertexArray(vaos[0]);
        gl.glDrawArrays(GL4.GL_TRIANGLES, 0, 3);
        gl.glBindVertexArray(0);
    }

    @Override
    public void dispose(GL4 gl) {

    }

    @Override
    public void reshape(GL4 gl, int x, int y, int width, int height) {

    }

    public static void main(String[] args){
        transform_01 engine = new transform_01();
        engine.setTitle("transform.transform_01");
        JOGLWrapper wrapper = new JOGLWrapper(engine);
    }
}
