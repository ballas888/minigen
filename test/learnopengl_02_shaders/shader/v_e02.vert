#version 450 core

layout (location = 0) in vec3 in_position;
layout (location = 1) in vec3 in_color;

uniform vec3 u_pos;

out vec3 out_color;
void main(void)
{
    gl_Position = vec4(in_position+u_pos,1.0);
    out_color = in_color;
}
