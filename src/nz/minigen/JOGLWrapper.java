package nz.minigen;

import com.jogamp.opengl.*;
import com.jogamp.opengl.awt.GLCanvas;
import com.jogamp.opengl.util.glsl.ShaderUtil;
import nz.minigen.input.Keyboard;
import nz.minigen.log.GLTraceLog;

import javax.swing.*;
import java.awt.*;
import java.nio.FloatBuffer;

/**
 * Created by Ballas on 1/12/2016.
 */
public class JOGLWrapper extends JFrame implements GLEventListener {
    private float updateTicksPerSecond;// = 25.0f;
    private float updateSkipTicks;// = 1000.0f/updateTicksPerSecond;
    private double updateNextTick = 0.0f;
    private int updateMaxFrameSkip = 5;
    private int updateLoops = 0;
    private int updateCount = 0;

    private float renderTicksPerSecond;// = 62.0f;
    private float renderSkipTicks;// = 1000.0f/renderTicksPerSecond;
    private double renderNextTick = 0.0f;
    private int renderMaxFrameSkip = 5;
    private int renderLoops = 0;
    private int renderCount = 0;
    private float interpolation = 0.0f;
    private long count = 0;

    private long startTime = 0;
    private boolean isRunning = false;

    private float[] clearColor = {0.392f, 0.584f, 0.929f, 1.0f};

    private AbstractCallback callback;
    private GLCanvas glcanvas;
    private WrapperConfig config;

    public JOGLWrapper(AbstractCallback cb){
        this(cb,new WrapperConfig());
    }

    public JOGLWrapper(AbstractCallback cb, WrapperConfig conf){
        super(cb.getTitle());
        callback = cb;
        config = conf;
        callback.setCanvasWidth(config.width);
        callback.setCanvasHeight(config.height);

        updateTicksPerSecond = config.updatePerSecond;
        updateSkipTicks = 1000.0f/updateTicksPerSecond;

        renderTicksPerSecond = config.renderPerSecond;
        renderSkipTicks = 1000.0f/renderTicksPerSecond;


        GLProfile profile = GLProfile.get(GLProfile.GL4);
        GLCapabilities capabilities = new GLCapabilities(profile);
        glcanvas = new GLCanvas(capabilities);
        glcanvas.addGLEventListener(this);
        glcanvas.addKeyListener(callback.keyboard);
        //glcanvas.setPreferredSize(new Dimension(config.width, config.height));

        getContentPane().add(glcanvas);

        setSize(config.width, config.height);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setVisible(true);
        setResizable(false);
        glcanvas.requestFocusInWindow();
    }

    public double time(){
        if(startTime == 0.0){
            startTime = System.nanoTime();
            return 0.0f;
        }
        long currentTime = System.nanoTime();
        return (currentTime - startTime)/1000000.0;
    }

    @Override
    public void init(GLAutoDrawable glAutoDrawable) {
        GL drawable = glAutoDrawable.getGL().getGL4();

        if(config.debug){
            try {
                // Debug ..
                drawable = drawable.getContext().setGL( GLPipelineFactory.create("com.jogamp.opengl.Debug", null, drawable, null) );
            } catch (Exception e) {e.printStackTrace();}
        }

        if(config.trace) {
            try {
                // Trace ..
                drawable = drawable.getContext().setGL( GLPipelineFactory.create("com.jogamp.opengl.Trace", null, drawable, new Object[] { new GLTraceLog(System.err)} ) );
            } catch (Exception e) {e.printStackTrace();}
        }

        GL4 gl = drawable.getGL4();

        //Vsync
        gl.setSwapInterval(config.vsync?1:0);

        //GL setting
        gl.glClearColor(0.392f, 0.584f, 0.929f, 1.0f);
        gl.glEnable(GL4.GL_DEPTH_TEST);

        System.out.println("------------------------------------------------------------------");
        System.out.println("Entering Initialization!");

        System.out.println("GL Profile: "+ gl.getGLProfile());
        System.out.println("GL: "+gl);
        System.out.println("GL Version: "+gl.glGetString(GL.GL_VERSION));
        System.out.println("Vsync: "+gl.getSwapInterval());
        System.out.println("isShaderCompilerAvailible: "+ ShaderUtil.isShaderCompilerAvailable(gl));

        System.out.println("Program Ready!");
        System.out.println("------------------------------------------------------------------\n");

        callback.init(gl);

        Thread loop = new Thread(new Runnable() {
            @Override
            public void run() {
                loop();
            }
        });
        loop.setName("Loop Thread");
        loop.start();
    }

    private void loop(){
        isRunning = true;
        updateNextTick = time();
        renderNextTick = time();
        double oldTime = time();

        while(isRunning){
            updateLoops = 0;
            renderLoops = 0;

            callback.updateKeyboard();

            while(time() > updateNextTick && updateLoops < updateMaxFrameSkip){
                update();
                updateNextTick+=updateSkipTicks;
                updateLoops++;
            }

            while(time() > renderNextTick && renderLoops < renderMaxFrameSkip){
                glcanvas.repaint();
                renderNextTick+=renderSkipTicks;
                renderLoops++;
            }

            if(time() - oldTime > 1000.0){
                //System.out.println("FPS: "+renderCount+" UPS: "+updateCount);
                setTitle(callback.getTitle()+"     FPS: "+renderCount+" UPS: "+updateCount);
                updateCount = 0;
                renderCount = 0;
                oldTime += 1000.0;
            }

        }
    }

    public void update(){
        callback.update();
        updateCount++;
    }

    @Override
    public void dispose(GLAutoDrawable glAutoDrawable) {
        isRunning = false;
        GL4 gl = glAutoDrawable.getGL().getGL4();
        callback.dispose(gl);
    }

    @Override
     public  void display(GLAutoDrawable glAutoDrawable) {
        GL4 gl = glAutoDrawable.getGL().getGL4();
        //gl.glClearBufferfv(GL4.GL_COLOR,0,clearColor,0);
        gl.glClear(GL4.GL_COLOR_BUFFER_BIT|GL4.GL_DEPTH_BUFFER_BIT);


        interpolation = (float)((time()+updateSkipTicks-updateNextTick)/updateSkipTicks);
        callback.render(gl,interpolation, (float)time(), count);
        count++;

        renderCount++;
    }

    @Override
    public void reshape(GLAutoDrawable glAutoDrawable, int x, int y, int width, int height) {
        GL4 gl = glAutoDrawable.getGL().getGL4();
        gl.glViewport(0,0,width,height);
        callback.setCanvasWidth(width);
        callback.setCanvasHeight(height);
        callback.reshape(gl,x,y,width,height);
    }
}
