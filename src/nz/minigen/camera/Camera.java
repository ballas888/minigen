package nz.minigen.camera;

import com.jogamp.common.nio.Buffers;
import org.joml.Matrix4f;
import org.joml.Vector3f;

import java.nio.FloatBuffer;

/**
 * Created by Richard on 27/03/2016.
 */
public class Camera {
    FloatBuffer fb = Buffers.newDirectFloatBuffer(16);
    Vector3f pos = new Vector3f();
    Vector3f prevPos = new Vector3f();
    Matrix4f view = new Matrix4f();

    public void update(){
        prevPos.set(pos);
    }

    public FloatBuffer getView(float inter){
        Vector3f d = new Vector3f(prevPos);

        d.lerp(pos,inter);
        view.setTranslation(d);
        return view.get(fb);
    }

    public FloatBuffer getView(){
        view.setTranslation(pos);

        return view.get(fb);
    }

    public void translate(float x, float y, float z){
        prevPos.set(pos);

        pos.add(x,y,z);
    }
}
