package nz.minigen;

/**
 * Created by Ballas on 1/12/2016.
 */
public class WrapperConfig {
    public int width = 800;
    public int height = 600;

    public boolean vsync = true;
    public int updatePerSecond = 25;
    public int renderPerSecond = 61;

    public boolean debug = true;
    public boolean trace = false;
}
