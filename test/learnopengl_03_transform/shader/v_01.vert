#version 450 core

layout (location = 0) in vec3 in_position;
layout (location = 1) in vec3 in_color;

uniform mat4 u_matrix;

out vec3 vs_color;
void main(void)
{
    gl_Position = u_matrix*vec4(in_position,1.0);
    vs_color = in_color;
}