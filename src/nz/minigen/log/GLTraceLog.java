package nz.minigen.log;

import java.io.*;

public class GLTraceLog extends PrintStream {

    int count = 0;

    public String getStackTrace(){
        String fullClassName = Thread.currentThread().getStackTrace()[5].getClassName();
        String className = fullClassName.substring(fullClassName.lastIndexOf(".") + 1);
        int lineNumber = Thread.currentThread().getStackTrace()[5].getLineNumber();
        return className+":"+lineNumber+"> ";
    }

    @Override
    public void print(String s) {
        String trim = s.trim();
        if(!trim.equals("\n")&&!trim.equals("")){
            System.err.printf(getStackTrace());
            super.print(s);
        }
    }

    public GLTraceLog(OutputStream out) {
        super(out);
    }

    public GLTraceLog(OutputStream out, boolean autoFlush) {
        super(out, autoFlush);
    }

    public GLTraceLog(OutputStream out, boolean autoFlush, String encoding) throws UnsupportedEncodingException {
        super(out, autoFlush, encoding);
    }

    public GLTraceLog(String fileName) throws FileNotFoundException {
        super(fileName);
    }

    public GLTraceLog(String fileName, String csn) throws FileNotFoundException, UnsupportedEncodingException {
        super(fileName, csn);
    }

    public GLTraceLog(File file) throws FileNotFoundException {
        super(file);
    }

    public GLTraceLog(File file, String csn) throws FileNotFoundException, UnsupportedEncodingException {
        super(file, csn);
    }
}
